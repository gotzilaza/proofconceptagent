json.array!(@agents) do |agent|
  json.extract! agent, :id, :reference_number, :unit, :floor, :bedrooms, :bathrooms, :area, :description, :sale_price, :rent_price, :condo, :google_drive_folder, :dropbox_folder, :images_urls
  json.url agent_url(agent, format: :json)
end
