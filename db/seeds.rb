# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Agent.delete_all

Agent.create([
	{ 
		reference_number: 'BS20001',
		unit: 10,
		floor: 1,
		bedrooms: 1,
		bathrooms: 1,
		area: 29,
		description: 'ที่ตั้งโครงการ ซอยศรีนคริทร์ 49 ถ.ศรีนครินทร์ แขวงหนองบอน เขตประเวศ กรุงเทพมหานคร พื้นที่โครงการ พื้นที่โครงการประมาณ 3-1-72.80 ไร่',
		sale_price:  2120000,
		rent_price: 10000,
		condo: 'แอสปาย รีสอร์ท' ,
		google_drive_folder: 'http://google_drive.com/one',
		dropbox_folder: 'http://dropbox.com/one',
		images_urls:  'http://upload.bkkcitismart.com/properties/38314/lowres/14013455773a4cd62f6ba0003e61ac3928dff59382.jpg'
	},
	{ 
		reference_number: 'BS20002',
		unit: 20,
		floor: 2,
		bedrooms: 3,
		bathrooms: 3,
		area: 198,
		description: 'เดอะ เม็ท คอนโดหรูใจกลางสาทร คอนโดสูง 66 ชั้น ใจกลางศูนย์ธุรกิจของกรุงเทพฯ เดินทางสะดวกสบายสู่รถไฟฟ้า BTS ช่องนนทรี, MRT ลุมพินี และBRT หรือไม่ว่าจะเป็นทางด่วน เป็นส่วนตัวด้วยระบบ ไพรเวทลิฟท์, สระว่ายน้ำขนาด 50 เมตร พร้อมด้วย cabanas & Jacuzzi, คอร์ทเทนนิส 2 คอร์ท, ห้องแอโรบิค, เซาน่า & สตรีม, playground, games room, ห้องสมุด ซื้อ ขาย หรือ เช่า คอนโด เดอะ เม็ท ติดต่อหาเรา Bangkok CitiSmart ได้ทันที เพื่อให้ผู้เชี่ยวชาญของเราได้แนะนำคอนโดให้กับท่าน',
		sale_price: 120000,
		rent_price: 12000,
		condo: 'คอนโด เดอะ เม็ท สาทร กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/two',
		dropbox_folder: 'http://dropbox.com/two',
		images_urls: 'http://upload.bkkcitismart.com/properties/38314/lowres/14013455773a4cd62f6ba0003e61ac3928dff59382.jpg'
	},
	{ 
		reference_number: 'BS20003',
		unit: 30,
		floor: 3,
		bedrooms: 1,
		bathrooms: 1,
		area: 41,
		description: 'โนเบิล รีมิกซ์ (Noble Remix) คอนโดติดรถไฟฟ้า BTS ทองหล่อ พร้อมทางเชื่อ Sky Walk เชื่อต่อตรงเข้าคอนโดได้ทันที สะดวกรวดเร็ว เท้าแทบไม่ติดพื้น ใจกลางสุขุมวิท ใกล้ทองหล่อ เอกมัย ตัวอาคารเน้นความโปร่งโล่งสบาย มีพื้นที่สีเขียวสวนส่วนกลาง สาธารณูปโภคและสิ่งอำนวยความสะดวกครบครัน อาทิ สระว่ายน้ำ, ฟิตเนส, ห้องสตีม เป็นต้น ซื้อ ขาย หรือ เช่า คอนโด โนเบิล รีมิกซ์ ติดต่อหาเรา Bangkok CitiSmart ได้ทันที เพื่อให้ผู้เชี่ยวชาญของเราได้แนะนำคอนโดให้กับท่าน',
		sale_price: 6670000,
		rent_price: 16000,
		condo: 'คอนโด โนเบิล รีมิกซ์ (สุขุมวิท 36) สุขุมวิท กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/three',
		dropbox_folder: 'http://dropbox.com/three',
		images_urls: 'http://upload.bkkcitismart.com/projects/420/lowres/img5_B20050824.jpg'
	},
	{ 
		reference_number: 'BS20004', 
		unit: 40,
		floor: 4,
		bedrooms: 2,
		bathrooms: 2,
		area: 92,
		description: 'ควอทโทร บาย แสนสิริ คอนโดหรู ทำเลเยี่ยม หนึ่งในทำเลที่ดีที่สุดใจกลางทองหล่อ ใกล้รถไฟฟ้า BTS ทองหล่อ และแหล่งช๊อปปิ้งชั้นนำมากมาย ดีไซน์ดีเยี่ยมเป็นที่หนึ่งในทุกรายละเอียด พร้อมวัสดุที่คัดสรรพิเศษจากภายนอกถึงภายใน พร้อมรับสิ่งอำนวยความสะดวกมากมาย ไม่ว่าจะเป้นสระว่ายน้ำ 2 สระ พร้อมสระเด็ก ห้องออกกำลังกาย สวนส่วนกลาง พร้อม Chill Out Terrace มั่นใจด้วยรบบรักษาความปลอดภัยในห้องพักด้วย Video Door Phone พร้อมรับบริการที่มีระดับเหนือใคร อาทิ บริการรับจอดรถ บริการรถรับส่งแบบ London Cab บริการผู้ช่วยส่วนตัว เป้นต้น ซื้อ ขาย หรือ เช่า คอนโด ควอทโทร บาย แสนสิริ ติดต่อหาเรา Bangkok CitiSmart ได้ทันที เพื่อให้ผู้เชี่ยวชาญของเราได้แนะนำคอนโดให้กับท่าน',
		sale_price: 1200000,
		rent_price: 75000,
		condo: 'คอนโด ควอทโทร บาย แสนสิริ ทองหล่อ กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/four',
		dropbox_folder: 'http://dropbox.com/four',
		images_urls: 'http://upload.bkkcitismart.com/properties/39668/lowres/1407310186cc15c94d90ec1d09d3ceccbd26fb78d7.jpeg'
	},
	{ 
		reference_number: 'BS20005',
		unit: 50,
		floor: 5,
		bedrooms: 1,
		bathrooms: 1,
		area: 35 ,
		description: 'ที่ตั้งโครงการ : ซอยสุขุมวิท 34 ถนนสุขุมวิท แขวงคลองตัน เขตคลองเตย กรุงเทพมหานคร ห่างจากสถานีรถไฟฟ้า BTS ทองหล่อ ประมาณ 120 เมตร ประเภทโครงการ : คอนโดมิเนียม สไตล์ Modern Art Deco สูง 28 ชั้น จำนวน 1 อาคาร เนื้อที่โครงการ : ประมาณ 2 ไร่ 16 ตารางวา จำนวนห้อง : ประมาณ 265 ยูนิต คาดว่าก่อสร้างแล้วเสร็จ : ปลายปี พ.ศ. 2557 ราคาเริ่มต้น : 5.5 ล้านบาท เจ้าของโครงการ : บริษัท เอสซี แอสเสท คอร์ปอเรชั่น จำกัด (มหาชน)',
		sale_price: 6570000,
		rent_price: 18000,
		condo: 'คอนโด เดอะ เครสท์ สุขุมวิท 34 ทองหล่อ กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/five',
		dropbox_folder: 'http://dropbox.com/five',
		images_urls: 'http://upload.bkkcitismart.com/properties/30035/lowres/13964926133b87e1e2e9409625f19ae22cb8e3d5f9.jpg'
	},
	{ 
		 reference_number: 'BS20006',
		 unit: 60,
		 floor: 6,
		 bedrooms: 1,
		 bathrooms: 1,
		 area: 45,
		 description: 'ที่ตั้งโครงการ : ถนนสุขุมวิท (ระหว่างซอยสุขุมวิท 34 และ 36) แขวงคลองตัน เขตคลองเตย กรุงเทพมหานคร ห่างจากสถานีรถไฟฟ้า BTS ทองหล่อ ประมาณ 100 เมตร ประเภทโครงการ : คอนโดมิเนียม สูง 28 ชั้น จำนวน 1 อาคาร เนื้อที่โครงการ : ประมาณ 1 ไร่ 2 งาน 76 ตารางวา ประเภทห้อง : 1-3 ห้องนอน พื้นที่ใช้สอย : 33.5-154 ตารางเมตร จำนวนห้อง : 216 ยูนิต คาดว่าก่อสร้างแล้วเสร็จ : พฤศจิกายน พ.ศ. 2556 ราคาเริ่มต้น : ประมาณ 5 ล้านบาท ค่ากองทุนส่วนกลาง : 500 บาท/ตารางเมตร ค่าบำรุงรักษาส่วนกลาง : 57 บาท/ตารางเมตร/เดือน เจ้าของโครงการ : บริษัท แสนสิริ จำกัด (มหาชน)',
		 sale_price: 3900000,
		 rent_price: 38000,
		 condo: 'คอนโด คีน บาย แสนสิริ สุขุมวิท กรุงเทพมหานคร',
		 google_drive_folder: 'http://google_drive.com/six',
		 dropbox_folder: 'http://dropbox.com/six',
		 images_urls: 'http://upload.bkkcitismart.com/properties/35908/lowres/1396433593630af93a6181151b016c55ff900d086b.jpg'
	},
	{ 
		reference_number: 'BS20007',
		unit: 70,
		floor: 7,
		bedrooms: 1,
		bathrooms: 1,
		area: 30,
		description: 'ติดทะเลสวยงาม',
		sale_price: 5510000,
		rent_price: 15000,
		condo: 'คอนโด คิว อโศก ถนนเพชรบุรี กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/seven',
		dropbox_folder: 'http://dropbox.com/seven',
		images_urls: 'http://upload.bkkcitismart.com/properties/39268/lowres/14059385162e0f7170128efc284e40931349924817.jpg'
	},
	{ 
		reference_number: 'BS20008',
		unit: 80,
		floor: 8,
		bedrooms: 1,
		bathrooms: 1,
		area: 55,
		description: 'ด้วยทำเลของโครงการที่ติดรถไฟฟ้า BTS พระโขนงเพียงแค่ 1- 2 ก้าว ทำให้ไม่ต้องห่วงเรื่องความปลอดภัย โดยเฉพาะคุณผู้หญิงที่อาจจะต้องกลับบ้านดึกๆ หน้าโครงการอยู่ริมถนนสุขุมวิท ซอยด้านข้าง(สุขุมวิท 44/1) สามารถทะลุไปยังถนนพระราม 4 เพื่อเข้าสู่ใจกลางเมืองอย่างสาทร หรือขึ้นทางด่วนที่กล้วยน้ำไท ถ้าเทียบถึงความสะดวกในการเดินทางนับว่าดีกว่าทั้งเอกมัยและทองหล่อเลยทีเดียว ทั้งที่ห่างจากเอกมัยและทองหล่อเพียงแค่ 1- 2 สถานีรถไฟฟ้าเท่านั้น ถ้าจะพูดถึงในแง่ของความคุ้มค่าก็น่าสนใจครับ ห้องของโครงการนี้เป็นแบบ Fully Furnished พร้อมเข้าอยู่ ถ้าจะเทียบกับคอนโดแถวเอกมัย-ทองหล่อ ซึ่งส่วนใหญ่จะเป็นห้...',
		sale_price: 9020000,
		rent_price: 23000,
		condo: 'คอนโด รึทึ่ม สุขุมวิท 44/1 สุขุมวิท กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/eight',
		dropbox_folder: 'http://dropbox.com/eight',
		images_urls: 'http://upload.bkkcitismart.com/properties/21163/lowres/img6_C06071105.jpg'
	},
	{ 
		reference_number: 'BS20009',
		unit: 90,
		floor: 9,
		bedrooms: 2,
		bathrooms: 2,
		area: 72,
		description: 'สวัสดีครับ ต่อเนื่องจากรีวิวครั้งที่แล้ว ที่โครงการ Aspire Rama 9 วันนี้เรายังคงมาดูโครงการที่น่าสนใจบนถนนรัชดาภิเษก NEW CBD แห่งใหม่ของกรุงเทพฯ กันต่อนะครับ ซึ่งนั่นก็คือ โครงการ Ivy Ampio (ไอวี่ แอมพิโอ) อีกหนึ่งโครงการคุณภาพของบริษัทพฤกษา เรียลเอสเตท  จำกัด (มหาชน)...',
		sale_price: 3200000,
		rent_price: 55000,
		condo: 'คอนโด ไอวี่ แอมพิโอ รัชดาภิเษก กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/nine',
		dropbox_folder: 'http://dropbox.com/nine',
		images_urls: 'http://upload.bkkcitismart.com/properties/38674/lowres/140289948160d84174cfcd465ab7d5bd787f9389c6.jpg'
	},
	{ 
		reference_number: 'BS20010',
		unit: 100,
		floor: 10 ,
		bedrooms: 2,
		bathrooms: 2,
		area: 217,
		description: 'บ้านเหมือนใหม่',
		sale_price: 9590000,
		rent_price: 45000,
		condo: 'อนโด คลาส หลังสวน หลังสวน กรุงเทพมหานคร',
		google_drive_folder: 'http://google_drive.com/ten',
		dropbox_folder: 'http://dropbox.com/ten',
		images_urls: 'http://upload.bkkcitismart.com/projects/1351/lowres/img9_B10090910.jpg'
	}
])