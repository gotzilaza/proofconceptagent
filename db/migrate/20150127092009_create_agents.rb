class CreateAgents < ActiveRecord::Migration
  def change
    create_table :agents do |t|
      t.text :reference_number
      t.integer :unit
      t.integer :floor
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :area
      t.text :description
      t.integer :sale_price
      t.integer :rent_price
      t.text :condo
      t.text :google_drive_folder
      t.text :dropbox_folder
      t.text :images_urls

      t.timestamps null: false
    end
  end
end
